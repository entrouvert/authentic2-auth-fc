# authentic2-auth-fc - authentic2 authentication for FranceConnect
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import pytest
import django_webtest

from django.contrib.auth import get_user_model
from django_rbac.utils import get_ou_model
from authentic2_auth_fc.models import FcAccount


CARTMAN_FC_INFO = {
    "token": {
        "access_token": "cartmane_access_token",
        "token_type": "Bearer",
        "expires_in": 1200,
        "id_token": "cartman_token_id"
    },
    "sub": "c11661ed00014db58149c8a886c8180d",
    "user_info": {
        "birthcountry": "99404",
        "birthdate": "2006-06-06",
        "birthplace": "southpark",
        "email": "ecartman@ou_southpark.org",
        "family_name": "CARTMAN",
        "gender": "male",
        "given_name": "Eric",
        "preferred_username": "CARTMAN",
        "sub": "c11661ed00014db58149c8a886c8180d"
    }
}


def create_user(**kwargs):
    User = get_user_model()
    password = kwargs.pop('password', None) or kwargs['username']
    federation = kwargs.pop('federation', None)
    user, created = User.objects.get_or_create(**kwargs)
    if password:
        user.set_password(password)
        user.save()

    if federation:
        create_fc_federation(user, federation)
    return user


def create_fc_federation(user, info):
    kwargs = {
        'user': user,
        'token': json.dumps(info['token']),
        'user_info': json.dumps(info['user_info']),
        'sub': info['sub']
    }
    return FcAccount.objects.create(**kwargs)


@pytest.fixture
def app(request, db):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp(extra_environ={'HTTP_HOST': 'localhost'})


@pytest.fixture
def fc_settings(settings):
    settings.A2_FC_ENABLE = True
    settings.A2_FC_CLIENT_ID = 'xxx'
    settings.A2_FC_CLIENT_SECRET = 'yyy'
    return settings


@pytest.fixture
def ou_southpark(db):
    OU = get_ou_model()
    return OU.objects.create(name='southpark', slug='southpark')


@pytest.fixture
def admin(db):
    return create_user(username='admin', is_superuser=True, is_staff=True)


@pytest.fixture
def user_cartman(db, ou_southpark):
    return create_user(username='ecartman', first_name='eric', last_name='cartman',
                       email='ecartman@southpark.org', ou=ou_southpark, federation=CARTMAN_FC_INFO)


class AllHook(object):
    def __init__(self):
        self.calls = {}
        from authentic2 import hooks
        hooks.get_hooks.cache.clear()

    def __call__(self, hook_name, *args, **kwargs):
        calls = self.calls.setdefault(hook_name, [])
        calls.append({'args': args, 'kwargs': kwargs})

    def __getattr__(self, name):
        return self.calls.get(name, [])

    def clear(self):
        self.calls = {}


@pytest.fixture
def hooks(settings):
    if hasattr(settings, 'A2_HOOKS'):
        hooks = settings.A2_HOOKS
    else:
        hooks = settings.A2_HOOKS = {}
    hook = hooks['__all__'] = AllHook()
    yield hook
    hook.clear()
    del settings.A2_HOOKS['__all__']
