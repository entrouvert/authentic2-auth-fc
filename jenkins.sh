#!/bin/bash

set -e

for DIRECTORY in "htmlcov" "htmlcov-coverage-dj18-authentic-pg" "htmlcov-coverage-dj18-rbac-pg" "venv"
do
    if [ -d "$DIRECTORY" ]; then
        rm -r $DIRECTORY
    fi
done

virtualenv venv
venv/bin/pip install tox
PGPORT=`python -c 'import struct; import socket; s=socket.socket(); s.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack("ii", 1, 0)); s.bind(("", 0)); print(s.getsockname()[1]); s.close()'` pg_virtualenv -o fsync=off venv/bin/tox -rv
