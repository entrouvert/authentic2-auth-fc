# authentic2-auth-fc - authentic2 authentication for FranceConnect
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys


class AppSettings(object):
    '''Thanks django-allauth'''
    __SENTINEL = object()

    def __init__(self, prefix):
        self.prefix = prefix

    def _setting(self, name, dflt=__SENTINEL):
        from django.conf import settings
        from django.core.exceptions import ImproperlyConfigured

        v = getattr(settings, self.prefix + name, dflt)
        if v is self.__SENTINEL:
            raise ImproperlyConfigured('Missing setting %r' % (self.prefix + name))
        return v

    @property
    def enable(self):
        return self._setting('ENABLE', False)

    @property
    def create(self):
        return self._setting('CREATE', False)

    @property
    def authorize_url(self):
        return self._setting('AUTHORIZE_URL', 'https://fcp.integ01.dev-franceconnect.fr/api/v1/authorize')

    @property
    def token_url(self):
        return self._setting('TOKEN_URL', 'https://fcp.integ01.dev-franceconnect.fr/api/v1/token')

    @property
    def userinfo_url(self):
        return self._setting('USERINFO_URL', 'https://fcp.integ01.dev-franceconnect.fr/api/v1/userinfo')

    @property
    def logout_url(self):
        return self._setting('LOGOUT_URL', 'https://fcp.integ01.dev-franceconnect.fr/api/v1/logout')

    @property
    def about_url(self):
        return self._setting('ABOUT_URL', 'https://app.franceconnect.gouv.fr/en-savoir-plus')

    @property
    def logout_when_unlink(self):
        return self._setting('LOGOUT_WHEN_UNLINK', True)

    @property
    def logout_at_unlink_return_url(self):
        return self._setting('LOGOUT_AT_UNLINK_RETURN_URL', '/accounts/')

    @property
    def enable_registration_form_prefill(self):
        return self._setting('ENABLE_REGISTRATION_FORM_PREFILL', True)

    @property
    def attributes_mapping(self):
        return self._setting('ATTRIBUTES_MAPPING',
                             {
                                 'family_name': 'last_name',
                                 'given_name': 'first_name',
                                 'email': 'email'
                             })

    @property
    def user_info_mappings(self):
        return self._setting('USER_INFO_MAPPINGS', {
            'last_name': {
                'ref': 'family_name',
                'verified': True,
            },
            'first_name': {
                'ref': 'given_name',
                'verified': True,
            },
            'email': 'email',
        })

    @property
    def next_field_name(self):
        return self._setting('NEXT_FIELD_NAME', 'fc_next')

    @property
    def client_id(self):
        return self._setting('CLIENT_ID')

    @property
    def client_secret(self):
        return self._setting('CLIENT_SECRET')

    @property
    def verify_certificate(self):
        return self._setting('VERIFY_CERTIFICATE', False)

    @property
    def client_credentials(self):
        return self._setting('CLIENT_CREDENTIALS', ())

    @property
    def show_button_quick_account_creation(self):
        return self._setting('SHOW_BUTTON_QUICK_ACCOUNT_CREATION', True)

    @property
    def auto_register(self):
        return self._setting('AUTO_REGISTER', True)

    @property
    def fd_list(self):
        return self._setting('FD_LIST', {})

    @property
    def scopes(self):
        return self._setting('SCOPES', [])

    @property
    def popup(self):
        return self._setting('POPUP', False)

app_settings = AppSettings('A2_FC_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
