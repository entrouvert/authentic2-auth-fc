# authentic2-auth-fc - authentic2 authentication for FranceConnect
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext_noop
from django.template.loader import render_to_string
from django.shortcuts import render

from authentic2 import app_settings as a2_app_settings, utils as a2_utils

from . import app_settings


class FcAuthenticator(object):
    def enabled(self):
        return app_settings.enable

    def name(self):
        return gettext_noop('FranceConnect')

    def id(self):
        return 'fc'

    @property
    def popup(self):
        return app_settings.popup

    def login(self, request, *args, **kwargs):
        if 'nofc' in request.GET:
            return
        fc_user_info = request.session.get('fc_user_info')
        context = kwargs.pop('context', {}).copy()
        params = {}
        if self.popup:
            params['popup'] = ''
        context.update({
            'popup': self.popup,
            'about_url': app_settings.about_url,
            'fc_user_info': fc_user_info,
        })
        if fc_user_info:
            context.update({
                'registration_url': a2_utils.make_url('fc-registration',
                                                      keep_params=True,
                                                      params=params,
                                                      request=request),
                'fc_user_info': fc_user_info,
            })
            template = 'authentic2_auth_fc/login_registration.html'
        else:
            context['login_url'] = a2_utils.make_url('fc-login-or-link',
                                                     keep_params=True,
                                                     params=params,
                                                     request=request)
            template = 'authentic2_auth_fc/login.html'
        return render(request, template, context)

    def profile(self, request, *args, **kwargs):
        # We prevent unlinking if the user has no usable password and can't change it
        # because we assume that the password is the unique other mean of authentication
        # and unlinking would make the account unreachable.
        unlink = request.user.has_usable_password() or a2_app_settings.A2_REGISTRATION_CAN_CHANGE_PASSWORD

        account_path = a2_utils.reverse('account_management')
        params = {
            'next': account_path,
        }
        if self.popup:
            params['popup'] = ''
        link_url = a2_utils.make_url('fc-login-or-link',
                                     params=params)

        context = kwargs.pop('context', {}).copy()
        context.update({
            'popup': self.popup,
            'unlink': unlink,
            'about_url': app_settings.about_url,
            'link_url': link_url,
        })
        return render_to_string('authentic2_auth_fc/linking.html', context, request=request)

    def registration(self, request, *args, **kwargs):
        if 'fc_user_info' in request.session:
            return []

        context = kwargs.get('context', {}).copy()
        params = {
            'registration': '',
        }
        if self.popup:
            params['popup'] = ''
        context.update({
            'login_url': a2_utils.make_url('fc-login-or-link',
                                           keep_params=True, params=params,
                                           request=request),
            'popup': self.popup,
            'about_url': app_settings.about_url,
        })
        return render(request, 'authentic2_auth_fc/registration.html', context)
