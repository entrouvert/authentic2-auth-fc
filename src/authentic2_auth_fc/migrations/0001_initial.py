# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FcAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sub', models.TextField(verbose_name='sub', db_index=True)),
                ('token', models.TextField(verbose_name='access token')),
                ('user_info', models.TextField(null=True, verbose_name='access token', blank=True)),
                ('user', models.ForeignKey(related_name='fc_accounts', verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
