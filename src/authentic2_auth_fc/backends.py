# authentic2-auth-fc - authentic2 authentication for FranceConnect
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

from authentic2.a2_rbac.utils import get_default_ou
from authentic2 import hooks

from . import models, app_settings, utils

logger = logging.getLogger(__name__)


class FcBackend(ModelBackend):
    def authenticate(self, sub, **kwargs):
        user_info = kwargs.get('user_info')
        user = None
        try:
            fc_account = models.FcAccount.objects.get(sub=sub, user__is_active=True)
            logger.debug(u'existing user %s using sub %s', fc_account.user, sub)
            user = fc_account.user
        except models.FcAccount.DoesNotExist:
            logger.debug(u'user with the sub %s does not exist.', sub)
        if user_info:
            if not user and app_settings.create:
                User = get_user_model()
                user = User.objects.create(ou=get_default_ou())
                fc_account = models.FcAccount.objects.create(
                    user=user,
                    sub=sub,
                    token=json.dumps(kwargs['token']))
                logger.debug(u'user creation enabled with fc_account (sub : %s - token : %s)',
                             sub, json.dumps(kwargs['token']))
                hooks.call_hooks('event', name='fc-create', user=user, sub=sub)
            if not user:
                return None
            logger.debug(u'updated (given_name : %s - family_name : %s)', user_info['given_name'],
                         user_info['family_name'])
            user.first_name = user_info['given_name']
            user.last_name = user_info['family_name']
            utils.apply_user_info_mappings(user, user_info)
            return user

    def get_saml2_authn_context(self):
        import lasso
        return lasso.SAML2_AUTHN_CONTEXT_PASSWORD_PROTECTED_TRANSPORT
