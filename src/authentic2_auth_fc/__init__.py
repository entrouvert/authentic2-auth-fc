# authentic2-auth-fc - authentic2 authentication for FranceConnect
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . import utils
from . import app_settings

from authentic2.utils import get_authentication_events

import django.apps


class Plugin(object):
    def get_before_urls(self):
        from . import urls
        return urls.urlpatterns

    def get_apps(self):
        return [__name__]

    def get_authentication_backends(self):
        return ['authentic2_auth_fc.backends.FcBackend']

    def get_authenticators(self):
        return ['authentic2_auth_fc.authenticators.FcAuthenticator']

    def redirect_logout_list(self, request, **kwargs):
        from django.core.urlresolvers import reverse

        url = utils.build_logout_url(request, next_url=reverse('auth_logout'))
        # url is assumed empty if no active session on the OP.
        if url:
            return [url]
        return []

    def registration_form_prefill(self, request):
        if app_settings.enable_registration_form_prefill:
            return [utils.get_mapped_attributes(request)]
        return []


class AppConfig(django.apps.AppConfig):

    name = __name__

    def ready(self):
        from .api_views import fc_unlink
        from authentic2.api_views import UsersAPI
        UsersAPI.fc_unlink = fc_unlink

    def a2_hook_api_modify_serializer(self, view, serializer):
        from rest_framework import serializers
        from authentic2.utils import make_url
        from . import app_settings

        if not app_settings.enable:
            return

        request = view.request

        if 'full' not in request.GET:
            return

        if view.__class__.__name__ == 'UsersAPI':
            def get_franceconnect(user):
                linked = user.fc_accounts.exists()
                return {
                    'linked': linked,
                    'link_url': make_url('fc-login-or-link', request=request, absolute=True),
                    'unlink_url': make_url('fc-unlink', request=request, absolute=True),
                }
            serializer.get_franceconnect = get_franceconnect
            serializer.fields['franceconnect'] = serializers.SerializerMethodField()

    def a2_hook_user_can_reset_password(self, user):
        if user.fc_accounts.exists():
            return True
        return None

    def a2_hook_user_can_change_password(self, user, request, **kwargs):
        if 'fc_id_token' in request.session:
            for authentication_event in get_authentication_events(request=request):
                if authentication_event['how'] == 'france-connect':
                    return False
        return True


default_app_config = '%s.%s' % (__name__, 'AppConfig')
