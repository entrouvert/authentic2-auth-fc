#!/usr/bin/python
# authentic2-auth-fc - authentic2 authentication for FranceConnect
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import sys
import os
import subprocess
import glob

from setuptools import setup, find_packages
from setuptools.command.install_lib import install_lib as _install_lib
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from distutils.cmd import Command


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        curdir = os.getcwd()
        try:
            os.environ.pop('DJANGO_SETTINGS_MODULE', None)
            from django.core.management import call_command
            for dir in glob.glob('src/*'):
                for path, dirs, files in os.walk(dir):
                    if 'locale' not in dirs:
                        continue
                    os.chdir(os.path.realpath(path))
                    call_command('compilemessages')
                    os.chdir(curdir)
        except ImportError:
            print()
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations')
            print()
            print()
            os.chdir(curdir)


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands


class eo_sdist(sdist):

    def run(self):
        print("creating VERSION file")
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print("removing VERSION file")
        if os.path.exists('VERSION'):
            os.remove('VERSION')


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(['git', 'describe', '--dirty=.dirty', '--match=v*'],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(
                subprocess.check_output(
                    ['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'

with open(os.path.join(os.path.dirname(__file__), 'README')) as fd:
    README = fd.read()

setup(
    name='authentic2-auth-fc',
    version=get_version(),
    license='AGPLv3',
    description='Authentic2 FranceConnect plugin',
    long_description=README,
    author="Entr'ouvert",
    url='https://repos.entrouvert.org/authentic2-auth-fc.git',
    author_email="info@entrouvert.com",
    packages=find_packages('src'),
    package_dir={
        '': 'src',
    },
    package_data={
        'authentic2_auth_fc': [
            'templates/authentic2_auth_fc/*.html',
            'static/authentic2_auth_fc/css/*.css',
            'static/authentic2_auth_fc/js/*.js',
            'static/authentic2_auth_fc/img/*.png',
            'static/authentic2_auth_fc/img/*.svg',
            'locale/fr/LC_MESSAGES/django.po',
            'locale/fr/LC_MESSAGES/django.mo',
            '*.json',
        ],
    },
    install_requires=[
        'authentic2',
        'requests>2.11',
        'requests-oauthlib',
    ],
    entry_points={
        'authentic2.plugin': [
            'authentic2-auth-fc = authentic2_auth_fc:Plugin',
        ],
    },
    cmdclass={
        'build': build,
        'install_lib': install_lib,
        'compile_translations': compile_translations,
        'sdist': eo_sdist},
    zip_safe=False,
)
